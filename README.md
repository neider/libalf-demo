# libALF Web Demo

This is a (somewhat outdated) copy of the [libALF web 
demo](http://libalf.informatik.rwth-aachen.de/). Precompiled libraries are 
included, located in the `lib` and `lib/native` folders.

This copy should run on both Windows and Linux with an installed Java runtime 
environment. As of May 2021, it has been tested on Ubuntu 20.04.2 LTS with 
openjdk 11.0.11.


## Prerequisites
The demo requires a Java runtime environment.


## Extracting the Native libALF Library
In order to run the demo, you first need to extract the native libALF library 
that matches your operating system and architecture. Currently, Windows and 
Linux on both i386 and amd64 are supported.

The native libraries are budeled as jar files, located in the `lib/native` 
directory. On Windows, the libraries have the file ending `.dll`, whereas the 
file ending on Linux is `.so`.

To extract the native libraries, proceed as follows:

1. Select the jar file matching your operating system and architecture (e.g., 
   `windows_amd64.jar` for 64-bit Windows).
2. Use `unzip` or any other decompression utility that understands the ZIP 
   format (jar files use this format) to extract the containing `.dll` or 
   `.so` file to a directory of your choice, say `native_path`.


## Running the Demo
The entry point of the demo is `de.libalf.demo.Starter`. Running the demo is 
straightforward and only requires two options:

- the `-Djava.library.path` option needs to point to the location of the native 
  libraries (i.e., to `native_path`); and
- the classpath `-cp` needs to include the file `jalf_demo.jar` as well as all 
  jar files in the `lib` folder.

For instance, executing

    java -Djava.library.path=native_path -cp "jalf_demo.jar:./lib/*" de.libalf.demo.Starter

on Linux should immediately starts the demo. Note that Windows users have to 
replace the colon with a semicolon in the definition of the classpath.